require('dotenv').config
const express = require('express')
const app = express()
const session = require('express-session')
const flash = require('express-flash')
const passport = require('./utils/passport')
const router = require('./routes')

// body parser
app.use(express.urlencoded({ extended: false }))

// session handler
app.use(session({
  secret: 'secret',
  resave: false,
  saveUninitialized: false
}))

// passport setting
app.use(passport.initialize())
app.use(passport.session())

// flash setting
app.use(flash())

// view engine setting
app.set('view engine', 'ejs')

// setting router
app.use(router)

app.listen(process.env.PORT, () => { console.log(`running at port ${process.env.PORT}`) })
