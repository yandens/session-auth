const { User } = require('../models')
const passport = require('../utils/passport')

const register = async (req, res, next) => {
  try {
    await User.register(req.body)

    return res.redirect('/auth/login')
  } catch (err) {
    next(err)
  }
}

const login = passport.authenticate('local', {
  successRedirect: '/',
  failureRedirect: '/login',
  failureFlash: true // active express flash
})

const whoami = (req, res, next) => {
  try {
    // req.user is from user model, it is the authenticate user from passport
    res.render('profiles', req.user.dataValues)
  } catch (err) {
    next(err)
  }
}

module.exports = { register, login, whoami }
