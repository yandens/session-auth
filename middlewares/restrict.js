module.exports = (req, res, next) => {
  // if user authenticated, go to next handler
  if (req.isAuthenticated()) return next()
  // if not
  res.redirect('/auth/login')
}
