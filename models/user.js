'use strict';
const {
  Model
} = require('sequelize')
const bcrypt = require('bcrypt')

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }

    // register
    static register = async ({ username, password }) => {
      try {
        const user = await this.findOne({ where: { username } })
        if (user) return Promise.reject('username already exist')

        const encrypted = await bcrypt.hash(password, 10)
        return this.create({ username, password: encrypted })
      } catch (err) {
        return Promise.reject(err)
      }
    }

    //login
    static login = async ({ username, password }) => {
      try {
        const user = await this.findOne({ where: { username } })
        const valid = await bcrypt.compare(password, user.password)

        if (!user || !valid) return Promise.reject('username or password wrong')
        return Promise.resolve(user)
      } catch (err) {
        return Promise.reject(err)
      }
    }

  }

  User.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};
