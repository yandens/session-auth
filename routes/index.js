const router = require('express').Router()
const auth = require('../controllers/auth')
const restrict = require('../middlewares/restrict')

// register
router.get('/auth/register', (req, res) => res.render('register'))
router.post('/auth/register', auth.register)

// login
router.get('/auth/login', (req, res) => res.render('login'))
router.post('/auth/login', auth.login)

// index
router.get('/', restrict, (req, res) => res.render('index'))

// whoami
router.get('/auth/whoami', restrict, auth.whoami)

module.exports = router
