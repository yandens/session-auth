const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const { User } = require('../models')

const authenticate = async (username, password, done) => {
  try {
    // login method from user models
    const user = await User.login({ username, password })
    return done(null, user)
  } catch (err) {
    return done(null, false, { message: err.message })
  }
}

passport.use(
  new LocalStrategy({ usernameField: 'username', passwordField: 'password' }, authenticate)
)

// create session
passport.serializeUser(
  (user, done) => done(null, user.id)
)

passport.deserializeUser(
  async (id, done) => done(null, await User.findByPk(id))
)

module.exports = passport
